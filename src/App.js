import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import uuid from 'react-uuid';
import moment from "moment";
import { Layout, Menu, Breadcrumb, Card, Button, Row, Col } from 'antd';
import ReactPlayer from 'react-player'
import AWS from 'aws-sdk';
import awsmobile from './aws-exports';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import  platform from 'platform';


var baseurl = "https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1"
//var baseurl = "http://localhost:3002/api/v1"

const { Header, Content, Footer } = Layout;
AWS.config.update({
  region: awsmobile.aws_cognito_region,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: awsmobile.aws_cognito_identity_pool_id
  })
});

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true,
      username: '',
      Group: '',
      Userinfo: '',
      email: '',
      password: '',
      start: false,
      end: false,
      src: '',
      data: {},
      id: '',
      videoplayed: false,
      status: {},
      enable: true,
      logoimg: '',
      cname: '',
      cntrl: true,
      ptoken: '',
      poster: '',
      usession: uuid(),
      watchtime: 0,
      allData: {},
      companyId: '',
      SubscriptionType: '',
      website: '',
      emialHeader: '',
      emialSubheader: '',
      companyName: '',
      CTA1Label: '',
      CTA1Url: '',
      CTA2Label: '',
      CTA2Url: '',
      isExplainerVideos: false,
      ExplainerVideos: [],
      UrlParameter: false,
      ExplainerUrl: '',
      uuid: uuid(),
      videoTitle: '',
      CatalogVideoId: '',
      LandingPageUrl: '',
      value:true,
      userLoggedIn: '',
      CTAbuttonDisabled: false
    };
  }

  async componentDidMount() {
    localStorage.setItem('watchtime', 0)
    localStorage.setItem('ntvid', 0)
    console.log("adata", window.init_data);
    console.log("ddata", window.page_type);
    window.addEventListener("resize", this.resize.bind(this));

    this.resize();
    var id = '';//const { params1 } = this.props.match
    if (window.page_type) {
      id = window.page_type;
      this.setState({ id })
    }
    else {
      id = this.props.match.params.id
      this.setState({ id })
    }
    console.log("QR",id)
    if(id.includes("QRcode")){
      let res = await axios.get(`${baseurl}/QRcode/${id.split("/")[1]}`)
      console.log("res",res.data)
      console.log("res",res.data.Items[0].CompanyId)
      console.log("res",res.data.Items[0].DestinationURL)
      let company = await axios.get(`${baseurl}/company/${res.data.Items[0].CompanyId}`);
      console.log("Explainer",company.data.Item)
      let newData =company?.data?.Item.Subscriptions.filter(d=>d.Type == "QRCode")
      console.log(newData)
      if(newData[0]?.Status == "Active"){
          var params = {
              Id:id.split("/")[1],
              Title:res.data.Items[0].Title,
              DestinationURL:res.data.Items[0].DestinationURL,
              isActive:true,
              //CompanyName:res.data.Items[0].CompanyName,
              CompanyId:res.data.Items[0].CompanyId,
              CreatedOn:res.data.Items[0].CreatedOn,
              QRRecord:res.data.Items[0].QRRecord ? Number(res.data.Items[0].QRRecord)+1 :"1",
              CreatedBy:res.data.Items[0].CreatedBy,
            }
         

             var anlytics =  await axios.post(`${baseurl}/QRcode/analytics`, params)
              console.log("res",anlytics)
              var info = platform.parse('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7.2; en; rv:2.0) Gecko/20100101 Firefox/4.0 Opera 11.52');
              // console.log("platform",info.description);
              // console.log("platform",info.os);
              // console.log("platform",platform.product);
              // console.log("platform",platform.os.family);
           
               //let data = getData()
              //   console.log("platform",getData())
              // async function getData(){
              //   return navigator.geolocation.getCurrentPosition(async function(position) {
              //     // console.log("Latitude is :", position.coords.latitude);
              //     // console.log("Longitude is :", position.coords.longitude);
              //     // Latitude=position.coords.latitude
              //     // Longitude=position.coords.longitude
              //     var obj = {
              //       Latitude: position.coords.latitude,
              //       Longitude:position.coords.longitude
              //     };
              //    return obj
              
              //   // });
              // //   var QRparams = {
              // //     QRId:id.split("/")[1],
              // //     Title:res.data.Items[0].Title,
              // //     DestinationURL:res.data.Items[0].DestinationURL,
              // //     //CompanyName:res.data.Items[0].CompanyName,
              // //     CompanyId:res.data.Items[0].CompanyId,
              // //     CreatedOn:res.data.Items[0].CreatedOn,
              // //     QRRecord:"1",
              // //     CreatedBy:res.data.Items[0].CreatedBy,
              // //    Device:platform?.product,
              // //    OS:platform?.os?.family,
              // //    Latitude:position.coords.latitude,
              // //    Longitude: position.coords.longitude
  
              // //   }
              // //  var QRanlytics =await axios.post(`${baseurl}/QRcode/QRanalytics`, QRparams)
              //   })
              //  }
              // // navigator.geolocation.getCurrentPosition(function(position) {
            
              //   console.log(position)
            
              // });
              var QRparams = {
                QRId:id.split("/")[1],
                Title:res.data.Items[0].Title,
                DestinationURL:res.data.Items[0].DestinationURL,
                isActive:true,
                //CompanyName:res.data.Items[0].CompanyName,
                CompanyId:res.data.Items[0].CompanyId,
                CreatedOn:res.data.Items[0].CreatedOn,
                QRRecord:"1",
                CreatedBy:res.data.Items[0].CreatedBy,
               Device:platform?.product,
               OS:platform?.os?.family,
              //  Latitude:position?.coords?.latitude,
              //  Longitude: position?.coords?.longitude
               Latitude:"",
               Longitude: ""

              }
             var QRanlytics =await axios.post(`${baseurl}/QRcode/QRanalytics`, QRparams)
             // console.log("Longitude is ::", dataFun);
            
             
              console.log("res",anlytics)
              
                 let CompanyURl = res.data.Items[0].DestinationURL
              CompanyURl.includes("https://") ? window.open(`${res.data.Items[0].DestinationURL}`,"_self") : window.open(`https://${res.data.Items[0].DestinationURL}`,"_self")
      }else{
          // window.open(`https://${company.data.Item.Website}`,"_self")
         // setvalue(true)
         this.setState({value:false})
      }
  
    }
  
    //console.log("params",this.props.match.params.id);
    // var uid=uuid().toString();
    //console.log(uid)
    //this.setState({usession:uid});
    console.log("s1", this.state);

    /* var kinesis = new AWS.Kinesis({region : 'us-east-1'});
      var currTime = new Date().getMilliseconds();
      var sensor = 'sensor-' + Math.floor(Math.random() * 100000);
      var reading = Math.floor(Math.random() * 1000000);
  
      var record = JSON.stringify({
        time : currTime,
        sensor : sensor,
        reading : reading
      });
  
      var recordParams = {
        Data : record,
        PartitionKey : sensor,
        StreamName : 'NEWCSTEST'
      };
  
      kinesis.putRecord(recordParams, function(err, data) {
        if (err) {
          console.log(err);
        }
        else {
          console.log('Successfully sent data to Kinesis.');
        }
      });*/

    try {



      var ExplainerRes = await axios.get(`${baseurl}/explainer/catalogRequestedVideosLandingPage/${id}`);
      console.log("ExplainerRes", ExplainerRes)
      //  var res = await axios.get(`${baseurl}/explainer/catalogRequestedVideosLandingPage/32e065c2-5274-420f-ac15-1f288d316e7e`);
      // console.log("Explainer",id.split("_")[0])
      // console.log("Explainer",id.split("_")[1])
      this.setState({
        UrlParameter: this.props.history.location.search.split('=')[2] == "true" ? true : false
      })
      if (ExplainerRes.data.Items.length > 0) {
        try {
          var company = await axios.get(`${baseurl}/company/${ExplainerRes.data.Items[0].CompanyId}`);
          console.log("Explainer", company.data.Item)
          let newData = company?.data?.Item.Subscriptions.filter(d => d.Type == "EXPLAINER")
          console.log(newData)
          if (newData[0]?.Status == "Active") {
            this.setState({
              ExplainerVideos: ExplainerRes.data.Items,
              companyName: ExplainerRes.data.Items[0].CompanyName,
              isExplainerVideos: true,
              videoTitle: ExplainerRes.data.Items[0].Title,
              emialHeader: ExplainerRes.data.Items[0]?.Header,
              emialSubheader: ExplainerRes.data.Items[0]?.SubHeader,
              CatalogVideoId: ExplainerRes.data.Items[0].CatalogVideoId,
              CompanyId: ExplainerRes.data.Items[0]?.CompanyId,
              VideoId: ExplainerRes.data.Items[0]?.VideoId,
              CTA1Label: ExplainerRes.data.Items[0]?.CTALabel,
              CTA2Label: ExplainerRes.data.Items[0]?.CTALabel2,
              CTA1Url: "https://" + ExplainerRes.data.Items[0]?.CTAUrl,
              CTA2Url: "https://" + ExplainerRes.data.Items[0]?.CTAUrl2,
              ExplainerUrl: ExplainerRes.data.Items[0]?.LandingUrlParameter,
              LandingPageUrl: `https://d24p9v2godfz48.cloudfront.net/processed/${ExplainerRes.data.Items[0]?.VideoId}/${ExplainerRes.data.Items[0]?.VideoId}.m3u8`
            })
            var recordParams = {
              info: '',
              UUID: this.state.uuid,
              companyName: ExplainerRes.data.Items[0].CompanyName,
              videoTitle: ExplainerRes.data.Items[0].Title,
              videoId: ExplainerRes.data.Items[0]?.VideoId,
              CatalogVideoId: ExplainerRes.data.Items[0].CatalogVideoId,
              landingpageloaded: true,
              videoplayed: false,
              CompanyId: ExplainerRes.data.Items[0]?.CompanyId
            };
            console.log("rec data", recordParams)
            var response = await axios.post(`${baseurl}/explainerAnalytics/create`, recordParams)
          } else window.open(`https://${company.data.Item.Website}`, "_self")
        } catch (error) {

        }
        //this.props.history.push(`/Explainer/${id}`)

      }

      try {
        var Logores2 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/company/getlogo/${ExplainerRes.data.Items[0].CompanyId}`);
        this.setState({ logoimg: "data:image/*;base64," + Logores2.data.base64 });
      }
      catch (e) {
      }

    } catch (error) {

    }

    try {

      var res = await axios.get("https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/enduser/" + id);
      console.log("res", res.data);
      var x = []
      x.push(res.data);
      this.setState({ BatchId: res.data.BatchId })

      this.setState({ data: res.data.Item })
      this.setState({ poster: "https://wardlaw-claims-prod-images.s3.amazonaws.com/WDGL_Imagelet_" + this.state.data.SubscriptionType + ".png" })
      try {
        var res2 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/company/getlogo/${res.data.Item.Company}`);
        this.setState({ logoimg: "data:image/*;base64," + res2.data.base64 });
      }
      catch (e) {
      }

      var params = {
        "TransactionId": id,
        "timezone": moment(new Date(), ["HH:mm A"]).format('HH')
      }

      try {
        //var res=await axios.post(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/sundaysky/SessionToken`,params)
        var res = await axios.post(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/sundaysky/SessionToken`, params)
        //var res=await axios.post(`http://localhost:3001/api/v1/sundaysky/SessionToken`,params)

        this.setState({ src: res.data });
        this.setState({ ptoken: res.data.playerToken })
        //console.log(this.src);
        //alert(this.state.ptoken)
      }
      catch (err) {
        //console.log(err);
      }
      try {
        var res3 = await axios.post(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/sundaysky/sessiontoken/Details`, params)
        //var res3 = await axios.post(`http://localhost:3001/api/v1/sundaysky/sessiontoken/Details`, params)
        this.setState({
          allData: res3.data.data,
          SubscriptionType: res3.data.data.SubscriptionType,
          companyId: res3.data.data.companyId,
          companyName: res3.data.data.companyName,
        })
      }
      catch (e) {
      }
      try {
        var res4 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/emailDetails/${this.state.data.SubscriptionType}`);
        //var res4 = await axios.get(`http://localhost:3001/api/v1/emailDetails/${this.state.data.SubscriptionType}`);
        this.setState({
          emialSubheader: res4.data.Items[0].SubHeader,
          emialHeader: res4.data.Items[0].Header
        })
        console.log("res4", res4)
      }
      catch (e) {
      }
      try {
        var res5 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/company/${this.state.data.Company}`)
        // var res5 = await axios.get(`http://localhost:3001/api/v1/company/${this.state.data.Company}`)
        this.setState({
          website: res5.data.Item.Website
        })
        console.log("website", res5.data.Item.Website)
      }
      catch (e) {
      }


      /*
      var params='';
      if(res.data.Item.SubscriptionType==="RENEWALS")
      {
        params = {
          "repositories": {
            "hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/ward-claims-demo/dev"
        },
        "story": {
            "hsrc": "Renewal",
            "repository": "story",
            "settings": {
                "output": {
                    "formats": {
                        "video": {
                            "format": "m3u8"
                        }
                    }
                }
            }
        },
      
          "data": {
              "dataElements": {
                /*  "CTABgColor":this.state.data.PrimaryColor1,
                  "CTAText": "Click",
                  "CTAUrl": "https://www.google.com",
                  "Data1": this.state.data.Email,
                  "Data2": this.state.data.Phone,
                  "Data3": this.state.data.SubscriptionType,
                  "FirstName": this.state.data.Name,
                  "FirstNameColor": this.state.data.PrimaryColor2,
                  "FirstNameUrl": "https://cdn.psv.one/assets/Hello_David.mp3",
                  "LogoUrl": "https://cdn.psv.one/assets/WardlawDigital/WARD_Digital_Logo_Lockup_Color_Logo.png"*/

      /*  "Primary1":res.data.Item.PrimaryColor1,
        "Primary2":res.data.Item.PrimaryColor2,
        "Accent1":res.data.Item.SecondaryColor1,
        "Accent2":res.data.Item.SecondaryColor2,
        "localTime24Hour": moment(new Date(), ["HH:mm A"]).format('HH'),
        "logo":  this.state.logoimg,
        "firstName": res.data.Item.Name,
        "MortgageLender":res.data.Item.FieldList.mlender,
        "PolicyAge": res.data.Item.FieldList.policyage,
        "PolicyRenewStart":moment(res.data.Item.FieldList.rdate).format('DD-MM-YYYY'),
        "PolicyRenewEnd":moment(res.data.Item.FieldList.edate).format('DD-MM-YYYY'),
        "PropertyCityState":res.data.Item.FieldList.add2?res.data.Item.FieldList.add1+","+res.data.Item.FieldList.add2:res.data.Item.FieldList.add1,
        "PropertyStreet": res.data.Item.FieldList.pstreet,
        "ShowFloodInsuranceScene": res.data.Item.FieldList.sfinsur, // Default false
        "ShowLawOrdinanceScene": res.data.Item.FieldList.slinsur, // Default false
        "ShowPortalScene": res.data.Item.FieldList.spinsur // Default false
    }
}
}

}
else
{
params = {
  "repositories": {
      "hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/test-program/dev"
     //"hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/ward-claims-demo/dev"
           
  },
  "story": {
      "hsrc": "WardlawDigital",
      "repository": "story",
      "settings": {
          "output": {
              "formats": {
                  "video": {
                      "format": "m3u8"
                  }
              }
          }
      }
  },
  "data": {
      "dataElements": {
        "CTABgColor":this.state.data.PrimaryColor1,
        "CTAText": "Click",
        "CTAUrl": "https://www.google.com",
         "Data1": this.state.data.ChannelType=='EMAIL'?this.state.data.Email:this.state.data.Phone,
       // "Data2": this.state.data.this.state.data.Phone,
        "Data3": this.state.data.SubscriptionType,
        "FirstName": this.state.data.Name,
        "FirstNameColor": this.state.data.PrimaryColor2,
        "FirstNameUrl": "https://cdn.psv.one/assets/Hello_David.mp3",
        "LogoUrl": "https://cdn.psv.one/assets/WardlawDigital/WARD_Digital_Logo_Lockup_Color_Logo.png"
      }
  }
}
}


}
catch(e)
{

}
console.log("111data",params)
try{
 
var res=await axios.post('https://api.sundaysky.com/vlx/video',JSON.stringify(params),{
headers: {
  'x-api-key': 'SF-00000380:dev:c2303d75376a2e76655301a96f5283b2',
 
  'Content-Type': 'application/json',
 
}
});
 
this.setState({src:res.data._links['video-stream'].href});
console.log("res",res.data._links['video-stream'].href);

*/
      var kinesis = new AWS.Kinesis({ region: 'us-east-1' });
      var currTime = new Date().getMilliseconds();
      var sensor = this.state.id;
      var reading = uuid();
      var format1 = "YYYY-MM-DD HH:mm:ss";
      var record = JSON.stringify({
        info: 0.0,
        // "UserSessionId":this.state.usession,
        // "TransactionId":sensor,
        "videoId": reading,
        "landingpageloaded": true,
        "videoplayed": false,
        // "CTAClicked":false,
        // "CTADisplayed":false,
        "creationdatetime": moment(new Date()).format(format1),
        "watchtime": 0,
        "NumberOfTimesVideoPlayed": 0,
        "BatchId": this.state.data.BatchId
      });
      //console.log("rec",record)
      var recordParams = {
        Data: {
          "videoId": reading,
          "landingpageloaded": true,
          "videoplayed": false,
          "creationdatetime": moment(new Date()).format(format1),
          "watchtime": 0,
          "NumberOfTimesVideoPlayed": 0
        },
        PartitionKey: reading,
        //StreamName : 'wardlaw'

      };
      console.log("rec", recordParams)
      // kinesis.putRecord(recordParams, function(err, data) {
      //   if (err) {
      //     console.log(err);
      //   }
      //   else {
      //     console.log(data,'Successfully sent data to Kinesis.');
      //   }
      // });

    }
    catch (err) {
      console.log(err);
      console.log("Error")

    }



  }
  handleProgress = async state => {
    if (this.state.start) {
      console.log('onProgress', JSON.stringify(state));



      //console.log("rec",record)
      var recordParams = {
        info: state,
        UUID: this.state.uuid,
        videoId: this.state.VideoId,
        companyName: this.state.companyName,
        videoTitle: this.state.videoTitle,
        CatalogVideoId: this.state.CatalogVideoId,
        landingpageloaded: true,
        videoplayed: this.state.videoplayed,
        CompanyId: this.state.CompanyId
      };
      console.log("rec data", recordParams)
      var response = await axios.post(`${baseurl}/explainerAnalytics/create`, recordParams)
    }
  }
  //   handleProgress =async state => {
  //     if(this.state.start)
  //     {
  //     console.log('onProgress', JSON.stringify(state));
  //     var kinesis = new AWS.Kinesis({region : 'us-east-1'});
  //     var currTime = new Date().getMilliseconds();
  //     var sensor =  this.state.id;
  //     var reading = uuid();

  //     var record = JSON.stringify({
  //       info:state,
  //       "TransactionId":sensor,
  //       "videoId":reading,
  //       "landingpageloaded":true,
  //       "videoplayed":this.state.videoplayed,
  //       "CTAClicked":false,
  //       "CTADisplayed":false,
  //       "BatchId":this.state.data.BatchId
  //     });
  // //console.log("rec",record)
  //     var recordParams = {
  //       Data : record,
  //       PartitionKey : reading,
  //       StreamName : 'wardlaw'
  //     };
  //     console.log("rec",recordParams)
  //     kinesis.putRecord(recordParams, function(err, data) {
  //       if (err) {
  //         console.log(err);
  //       }
  //       else {
  //         console.log('Successfully sent data to Kinesis.',data);
  //       }
  //     });


  //     }
  //     if(state.played===1)
  //     {
  //       this.setState({status:state})
  //       this.setState({end:true});
  //     }
  //   }
  test = async event => {
    var myPlayer = document.getElementById('sskyplayer');
    var id = this.state.id;
    var BatchId = this.state.BatchId;
    var usession = this.state.usession;

    //myPlayer.addEventListener('playStart', function(event) {


    //  });

    var nvid = localStorage.getItem('ntvid');
    var timesec = localStorage.getItem('watchtime');
    myPlayer.addEventListener('progress', function (event) {

      if (event.detail.position == '0.0') {
        // Do Something

        // alert("here");

        nvid++;
        localStorage.setItem('ntvid', nvid);

      }
      timesec++;
      console.log("wtime" + document.getElementsByClassName("vjs-current-time-display")[0].innerText);
      localStorage.setItem('watchtime', document.getElementsByClassName("vjs-current-time-display")[0].innerText);
      console.log("time in sec" + document.getElementsByClassName("vjs-current-time-display")[0].innerText);
      //console.log('onProgress', JSON.stringify(state));
      var kinesis = new AWS.Kinesis({ region: 'us-east-1' });
      var currTime = new Date().getMilliseconds();
      var sensor = id;
      var reading = uuid();
      var format1 = "YYYY-MM-DD HH:mm:ss";
      var record = JSON.stringify({
        info: event.detail.position,
        "UserSessionId": usession,
        "TransactionId": sensor,
        "videoId": reading,
        "landingpageloaded": true,
        "videoplayed": true,
        "CTAClicked": false,
        "CTADisplayed": false,
        "watchtime": document.getElementsByClassName("vjs-current-time-display")[0].innerText,
        "NumberOfTimesVideoPlayed": nvid,
        "creationdatetime": moment(new Date()).format(format1),
        "BatchId": BatchId
      });
      //console.log("rec",record)
      var recordParams = {
        Data: record,
        PartitionKey: reading,
        StreamName: 'wardlaw'
      };
      console.log("rec", recordParams)
      kinesis.putRecord(recordParams, function (err, data) {
        if (err) {
          console.log(err);
        }
        else {
          console.log('Successfully sent data to Kinesis.');
        }
      });
      //   alert("eeee1"+event.detail.position);

    });
    myPlayer.addEventListener('ctaClicked', function (event) {
      console.log("xxx", event.detail)
    });
  }
  handlePlay = async event => {
    this.setState({ start: true });
    this.setState({ videoplayed: true })
  }
  handleckick = event => {
    if (this.state.end) {
      //   alert("CTA Clicked");
      var kinesis = new AWS.Kinesis({ region: 'us-east-1' });
      var currTime = new Date().getMilliseconds();
      var sensor = this.state.id;
      var reading = uuid();

      var record = JSON.stringify({
        info: this.state.status,
        "TransactionId": sensor,
        "videoId": reading,
        "landingpageloaded": true,
        "videoplayed": this.state.videoplayed,
        "CTAClicked": true,
        'CTADisplayed': true,
        "BatchId": this.state.data.BatchId
      });
      //console.log("rec",record)
      this.setState({ enable: false })
      var recordParams = {
        Data: record,
        PartitionKey: reading,
        StreamName: 'wardlaw'
      };
      console.log("rec", recordParams)
      kinesis.putRecord(recordParams, function (err, data) {
        if (err) {
          console.log(err);
        }
        else {

          console.log('Successfully sent data to Kinesis.');
        }
      });

    }
  }
  resize() {
    let currentHideNav = (window.innerWidth <= 760);
    if (currentHideNav !== this.state.hideNav) {
      this.setState({ hideNav: currentHideNav });
      console.log('1resize', this.state.hideNav);
    }
  }
  
    onSubmitCTA1 = async () => {
    // console.log(platform?.product);
    // console.log("ogggggs",
    //   platform.os.family

    // )
    this.setState({ CTAbuttonDisabled: true })
    let params = {
      videoId: this.state.id,
      videoTitle: this.state.videoTitle,
      CatalogVideoId: this.state.CatalogVideoId,
      CTA1: true,
      CTALable1: this.state.CTA1Label,
      CTADestURL1: this.state.CTA1Url,
      CTA2: "",
      CTALable2: "",
      CTADestURL2: "",
      Device: platform?.product,
      os: platform.os.family,
      // os: window.navigator.userAgentData.platform,

      CompanyName: this.state.companyName,
      CreatedOn: Date.now()
    }
    await axios.post(`${baseurl}/CTA/CTAanalytics`, params)
      .then((result) => {
        console.log(result)
        if (result) {
          axios.post(`${baseurl}/cta1Analytics/${this.state.id}`,
            {
              videoId: this.state.id,
              videoTitle: this.state.videoTitle,
              CatalogVideoId: this.state.CatalogVideoId,
              CTA1: true,
              CTALable1: this.state.CTA1Label,
              CTADestURL1: this.state.CTA1Url,
              CTA2: "",
              CTALable2: "",
              CTADestURL2: "",
              CompanyName: this.state.companyName,
              CompanyId: this.state.CompanyId,
              CreatedOn: Date.now()
            }
          )
            .then(async (countResult) => {
              // await window.open(this.state.CTA1Url ? this.state.CTA1Url.replace("https://https://", 'https://') : '', "_blank")
              // console.log(countResult)
            })
            .catch((error) => {
              console.log(error)
            })
          window.open(this.state.CTA1Url ? this.state.CTA1Url.replace("https://https://", 'https://') : '', "_blank")
          this.setState({ CTAbuttonDisabled: false })
        }
      }).catch((err) => {
        console.log(err)
      })
  }
   onSubmitCTA2 = async () => {
    this.setState({ CTAbuttonDisabled: true })
    let params = {
      videoId: this.state.id,
      videoTitle: this.state.videoTitle,
      CatalogVideoId: this.state.CatalogVideoId,
      CTA1: "",
      CTALable1: "",
      CTADestURL1: "",
      CTA2: true,
      CTALable2: this.state.CTA2Label,
      CTADestURL2: this.state.CTA2Url,
      Device: platform?.product,
      os: platform.os.family,
      CompanyName: this.state.companyName,
      CreatedOn: Date.now()
    }
    await axios.post(`${baseurl}/CTA/CTAanalytics`, params)
      .then((result) => {
        console.log(result)
        if (result) {
          axios.post(`${baseurl}/cta2Analytics/${this.state.id}`,
            {
              videoId: this.state.id,
              videoTitle: this.state.videoTitle,
              CatalogVideoId: this.state.CatalogVideoId,
              CTA1: "",
              CTALable1: "",
              CTADestURL1: "",
              CTA2: true,
              CTALable2: this.state.CTA2Label,
              CTADestURL2: this.state.CTA2Url,
              CompanyName: this.state.companyName,
              CompanyId: this.state.CompanyId,
              CreatedOn: Date.now()
            }
          )
            .then(async (countResultforCTA2) => {
              // await window.open(this.state.CTA2Url ? this.state.CTA2Url.replace("https://https://", 'https://') : '', "_blank")
              // console.log(countResultforCTA2)
            })
            .catch((error) => {
              console.log(error)
            })
          window.open(this.state.CTA2Url ? this.state.CTA2Url.replace("https://https://", 'https://') : '', "_blank")
          this.setState({ CTAbuttonDisabled: false })
        }

      }).catch((err) => {
        console.log(err)
      })
  }
  
  
  
  
  
  render() {
    if(this.props.match.params.id.includes("QRcode")){
      alert("Yes")
      return <Redirect to='/QRcodes'/>
    }
    console.log("adata", window);
    console.log("adataa", this.state);
    const { ExplainerVideos, isExplainerVideos, logoimg, website, companyName, UrlParameter,value } = this.state
    return (
      <>
     
        {isExplainerVideos ?
          UrlParameter ?
            <Layout style={{ backgroundColor: 'white' }}>
              <Header calssName="center" style={{ position: 'fixed', zIndex: 1, width: '100%', boxShadow: "2px 5px 9px #888887", backgroundColor: "white" }}>
                <div className="logo center" >{logoimg ? <img src={logoimg} style={{ height: "50px", width: "auto", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "6px" }} /> :
                  <h4 style={{ marginTop: "9px" }} calssName="center">{companyName}</h4>}</div>
              </Header>

              <div style={{ display: "flex", justifyContent: 'center', marginTop: "80px" }}>


                {ExplainerVideos ? ExplainerVideos.map(data => {

                  return (
                    <div className="ant-col ant-col-xs-20 ant-col-sm-20 ant-col-xl-12" key={data.Id}>
                      {/* <Card bordered={false} style={{ width: 323 }}> */}
                      <h4 style={{ display: "flex", justifyContent: 'center' }}>{data.Title}</h4>
                      <h3 className="hide-on-small-only" style={{ fontSize: "22px", color: '#87CEFA', textAlign: "center" }}><span style={{ color: "#4682B4" }}></span>{" " + this.state.emialHeader}</h3>
                      <h3 className="hide-on-med-and-up" style={{ fontSize: "18px", color: '#87CEFA', textAlign: "center" }}><span style={{ color: "#4682B4" }}></span>{" " + this.state.emialHeader}</h3>

                      <p>{data.Description && data.Description}</p>
                      {/* {reactElement ?  reactElement : 
                        null
                        
                    } */}
                      <ReactPlayer
                        // ref={this.ref}
                        className='react-player'
                        url={this.state.LandingPageUrl}
                        //  url="https://explainer-video.s3.amazonaws.com/Bluejay-SunLifeOnboarding.mp4"
                        // url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                        playsinline
                        controls={this.state.cntrl}

                        onProgress={this.handleProgress}
                        onPlay={this.handlePlay}
                        onClick={this.handleckick}
                        //width='100%'
                        //  height='89%'
                        width={"100%"}
                      // height={"100vh"}
                      // style={{marginTop:"54px"}}
                      />


                    </div>

                  )
                }) : value && <div><img src={require("./220.gif")} /><p style={{ paddingLeft: "62px" }}>Loading...</p></div>}


              </div>
              <br></br>
              <Row style={{ marginTop: "1.5em" }}>
                <Col span={6} offset={6} xs={{ order: 1 }} sm={{ order: 1 }} md={{ order: 2 }} lg={{ order: 2 }}>
                        {this.state.CTA1Label ?
                    <div className="col s11 offset-s1"  style={{ width: "auto", marginLeft: "auto" }}>
                      <Button onClick={this.onSubmitCTA1}
                        disabled={this.state.CTAbuttonDisabled}
                        style={{ fontSize: '18px', background: "teal", height: '48px', letterSpacing: "1px", width: "90%", color: "white", borderRadius: "20px" }}>{this.state.CTA1Label && this.state.CTA1Label}</Button>
                    </div> : ''}

                </Col>
                <Col style={{ marginRight: "auto", marginLeft: "2.8em" }} span={6} xs={{ order: 1 }} sm={{ order: 1 }} md={{ order: 3 }} lg={{ order: 3 }}>
            {this.state.CTA2Label ?
                    <div className="col s11 offset-s1">
                      <Button onClick={this.onSubmitCTA2}
                        disabled={this.state.CTAbuttonDisabled}
                        style={{ fontSize: '18px', background: "teal", height: '48px', letterSpacing: "1px", width: "90%", color: "white", borderRadius: "20px" }}>{this.state.CTA2Label && this.state.CTA2Label}</Button>
                    </div> : ""}
                </Col>
              </Row>
              {/* <div className="row hide-on-med-and-up">
               {this.state.CTA1Label ? 
                <div className="col s11 offset-s1">
                <Button onClick={() => window.open(this.state.CTA1Url ? this.state.CTA1Url.replace("https://https://",'https://') : '', "_blank")}
                  style={{ fontSize: '18px',background:"teal", height:'48px', letterSpacing: "1px",margin: "10px auto",width:"91%", color: "white", borderRadius: "20px"}}>{this.state.CTA1Label && this.state.CTA1Label}</Button>
             </div> : ''}
               {this.state.CTA2Label ? 
              <div className="col s11 offset-s1">
              <Button onClick={() => window.open(this.state.CTA2Url ? this.state.CTA2Url.replace("https://https://",'https://') : '', "_blank")}
                
                    style={{ fontSize: '18px', background:"teal", height:'48px', letterSpacing: "1px", width: "91%", color: "white", borderRadius: "20px" }}>{this.state.CTA2Label && this.state.CTA2Label}</Button>    
              </div> : ""}
          
           </div>
           <div className="row hide-on-small-only">
               {this.state.CTA1Label ?
                 <div className="col s6">
                   <Button onClick={() => window.open(this.state.CTA1Url ?
                     this.state.CTA1Url.replace("https://https://",'https://') : '', "_blank")}
                     style={{fontSize: '18px', height: '48px',background:"teal", letterSpacing: "1px", width: "100%",color: "white", borderRadius: "20px"
                     }}>{this.state.CTA1Label && this.state.CTA1Label}</Button>
                 </div> : ""}
           {this.state.CTA2Label ?
           <div className="col s6">
                 <Button onClick={() => window.open(this.state.CTA2Url ?
                   this.state.CTA2Url.replace("https://https://",'https://') : '', "_blank")}
                   style={{
                     fontSize: '18px', background:"teal", height: '48px', letterSpacing: "1px",width: "100%",
                     color: "white", borderRadius: "20px"
                   }}>{this.state.CTA2Label && this.state.CTA2Label}</Button>    
           </div> : ""}
        </div> */}
              <br></br>

              <div style={{ display: "flex", justifyContent: 'center' }}>
                <p className="hide-on-med-and-up" style={{ width: "85%", color: "grey", textAlign: 'left', padding: "15px", fontSize: "18px", maxWidth: "750px" }}>
                  {this.state.emialSubheader}</p>

                <p className="hide-on-small-only" style={{ width: "54%", color: "grey", textAlign: 'left', padding: "20px", fontSize: "18px" }}>
                  {this.state.emialSubheader}</p>

              </div>
              <Footer style={{ textAlign: 'center', fontSize: '18px' }}>
                <p style={{ cursor: 'pointer', color: "blue" }} onClick={() => window.open(`https://${website}`, "_blank")}>{website}</p>
                ©{(new Date().getFullYear())} {this.state.companyName}. All rights reserved</Footer>
            </Layout> :
            <>
              <div style={{ display: "flex", justifyContent: 'center' }}>


                {ExplainerVideos ? ExplainerVideos.map(data => {

                  return (
                    <div className="ant-col ant-col-xs-24 ant-col-sm-24 ant-col-xl-24" key={data.Id}>
                      {/* <Card bordered={false} style={{ width: 323 }}> */}
                      {/* {reactElement ?  
                 <ReactPlayer
                 ref={this.ref}
                 className='react-player'
               
                // url="https://explainer-video.s3.amazonaws.com/Bluejay-SunLifeOnboarding.mp4"
                 url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                 playsinline
                 controls={this.state.cntrl}
               
                 onProgress={this.handleProgress}
                 onPlay={this.handlePlay}
                 onClick={this.handleckick}
                 width='100%'
                 height='89%'
                 style={{marginTop:"54px"}}
         />
   : 
                 null
             } */}
                      <ReactPlayer
                        // ref={this.ref}
                        className='react-player'
                        url={this.state.LandingPageUrl}
                        // url="https://explainer-video.s3.amazonaws.com/Bluejay-SunLifeOnboarding.mp4"
                        // url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                        playsinline
                        controls={this.state.cntrl}

                        onProgress={this.handleProgress}
                        onPlay={this.handlePlay}
                        onClick={this.handleckick}
                        //width='100%'
                        //  height='89%'
                        width={"100%"}
                        height={"100vh"}
                        style={{ marginTop: "54px" }}
                      />
                    </div>

                  )
                }) :  value && <div><img src={require("./220.gif")} /><p style={{ paddingLeft: "62px" }}>Loading...</p></div>}
                </div>
            </>
          :
          <Layout style={{ backgroundColor: 'white' }}>
            <Header calssName="center" style={{ position: 'fixed', zIndex: 1, width: '100%', boxShadow: "2px 5px 9px #888887", backgroundColor: "white" }}>
              <div className="logo center" >{this.state.logoimg ? <img src={this.state.logoimg} style={{ height: "50px", width: "auto", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "6px" }} /> :
                <h4 style={{ marginTop: "9px" }} calssName="center">{this.state.data.CompanyName}</h4>}</div>
            </Header>

            <Content className="site-layout" style={{
              marginLeft: "1px",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover"
            }}><br />

              {/* <div className="site-layout-background" style={{ padding: 24 }}>*/}
              {/* <Card style={{ width: "785px",boxShadow: "14px 12px 9px #888887",marginLeft: "116px"}} >*/}
              <div className="player-wrapper" style={{
                margin: "auto",
                width: "90%",
                maxWidth: "720px",
                height: "auto",
                /*paddingLeft: this.state.src?"184%":"67%",
                paddingRight: "73%",
                marginLeft: "-78%",*/
                paddingTop: this.state.hideNav ? "60px" : "4%"
              }}>{/*{this.state.src?
       <ReactPlayer
               ref={this.ref}
               className='react-player'
             
               url={this.state.src.data}
               playsinline
               controls={this.state.cntrl}
             
               onProgress={this.handleProgress}
               onPlay={this.handlePlay}
               onClick={this.handleckick}
               width='100%'
               height='89%'
               style={{marginTop:"54px"}}
       />*/}
                <br></br>
                {this.state.ptoken != '' ?
                  <div>
                    <h3 className="hide-on-small-only" style={{ fontSize: "22px", color: '#87CEFA', textAlign: "center" }}><span style={{ color: "#4682B4" }}>Hi {this.state.allData.firstName}, </span>{" " + this.state.emialHeader}</h3>
                    <h3 className="hide-on-med-and-up" style={{ fontSize: "18px", color: '#87CEFA', textAlign: "center" }}><span style={{ color: "#4682B4" }}>Hi {this.state.allData.firstName}, </span>{" " + this.state.emialHeader}</h3>

                    <sundaysky-video id="sskyplayer" analytics-token="SF-00000562" session={this.state.ptoken}
                      poster={this.state.poster} lang="en" className='react-player' style={{ position: "relative", width: "100%" }}
                      onClick={this.test}></sundaysky-video>
                  </div>

                  : 
                  value && <div style={{
                    marginLeft: this.state.hideNav ? "1%" : "45%",
                    /* margin-bottom: -4px; */
                    marginTop: "-25%"
                  }}><img src={require("./220.gif")} /><p style={{ paddingLeft: "62px" }}>Loading...</p></div>}</div>
              {/*</Card>
   
     
   </div>*/}
              <br></br>
              {this.state.ptoken != '' ?
                <div>
                  <div className="row hide-on-med-and-up">
                    {this.state.allData.Cta1Label ?
                      <div className="col s11 offset-s1">
                        <Button onClick={() => window.open(this.state.allData.Cta1Url ? this.state.allData.Cta1Url.replace("https://https://", 'https://') : '', "_blank")}
                          style={{ fontSize: '18px', height: '48px', letterSpacing: "1px", margin: "10px auto", width: "91%", backgroundColor: this.state.allData && this.state.allData.Primary1, color: "white", borderRadius: "20px" }}>{this.state.allData.Cta1Label && this.state.allData.Cta1Label}</Button>
                      </div> : ''}
                    {this.state.allData.Cta2Label ?
                      <div className="col s11 offset-s1">
                        <Button onClick={() => window.open(this.state.allData.Cta2Url ? this.state.allData.Cta2Url.replace("https://https://", 'https://') : '', "_blank")}

                          style={{ fontSize: '18px', height: '48px', letterSpacing: "1px", backgroundColor: this.state.allData && this.state.allData.Primary2, width: "91%", color: "white", borderRadius: "20px" }}>{this.state.allData.Cta2Label && this.state.allData.Cta2Label}</Button>
                      </div> : ""}

                  </div>
                  <div className="row hide-on-small-only">
                    {this.state.allData.Cta1Label ?
                      <div className="col s6 ">
                        <Button className="right" onClick={() => window.open(this.state.allData.Cta1Url ?
                          this.state.allData.Cta1Url.replace("https://https://", 'https://') : '', "_blank")}
                          style={{
                            fontSize: '18px', height: '48px', letterSpacing: "1px", width: "52%",
                            backgroundColor: this.state.allData && this.state.allData.Primary1, color: "white", borderRadius: "20px"
                          }}>{this.state.allData.Cta1Label && this.state.allData.Cta1Label}</Button>
                      </div> : ""}
                    {this.state.allData.Cta2Label ?
                      <div className="col s6">
                        <Button onClick={() => window.open(this.state.allData.Cta2Url ?
                          this.state.allData.Cta2Url.replace("https://https://", 'https://') : '', "_blank")}
                          style={{
                            fontSize: '18px', height: '48px', letterSpacing: "1px",
                            width: "52%", backgroundColor: this.state.allData && this.state.allData.Primary2,
                            color: "white", borderRadius: "20px"
                          }}>{this.state.allData.Cta2Label && this.state.allData.Cta2Label}</Button>
                      </div> : ""}
                  </div>
                </div> : ''}

            </Content>
            <br></br>
            <div style={{ display: "flex", justifyContent: 'center' }}>
              <p className="hide-on-med-and-up" style={{ width: "85%", color: "grey", textAlign: 'left', padding: "15px", fontSize: "18px", maxWidth: "750px" }}>
                {this.state.emialSubheader}</p>

              <p className="hide-on-small-only" style={{ width: "54%", color: "grey", textAlign: 'left', padding: "20px", fontSize: "18px" }}>
                {this.state.emialSubheader}</p>

            </div>
            <br></br>
            {this.state.ptoken != '' ?
              <Footer style={{ textAlign: 'center', fontSize: '18px' }}>
                <p style={{ cursor: 'pointer', color: "blue" }} onClick={() => window.open(`https://${this.state.website}`, "_blank")}>{this.state.website}</p>
                ©{(new Date().getFullYear())} {this.state.companyName}. All rights reserved</Footer> : ''}
          </Layout>}
          {!value && <h4 style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop:"20%"
        }}>Under Maintenance... </h4>}
      </>
    );
  }
}

export default App;
